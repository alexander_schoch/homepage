import Image from "next/image";

import { useRef } from "react";

import { Carousel } from "@mantine/carousel";
import Autoplay from "embla-carousel-autoplay";

import { Paper } from "@mantine/core";

function ImageCarousel({ images, setOpen }) {
  const autoplay = useRef(Autoplay({ delay: 6000 }));

  return (
    <Paper shadow="md" radius="md" sx={{ overflow: "hidden" }}>
      <Carousel
        loop
        plugins={[autoplay.current]}
        onMouseEnter={autoplay.current.stop}
        onMouseLeave={autoplay.current.reset}
        slideGap="md"
      >
        {images.map((image, i) => (
          <Carousel.Slide key={i} style={{ display: "flex" }}>
            <Image
              src={"/project_images/" + image}
              width={0}
              height={0}
              sizes="100vw"
              style={{ width: "100%", height: "auto" }}
              alt="Application Screenshot"
              onClick={setOpen}
            />
          </Carousel.Slide>
        ))}
      </Carousel>
    </Paper>
  );
}

export default ImageCarousel;
