import { useTranslation } from "next-i18next";

import { Button, Group, Text, useMantineTheme } from "@mantine/core";

import { IconBrandMastodon, IconHome, IconCode } from "@tabler/icons-react";

export default function Impressum() {
  const { t } = useTranslation("footer");
  const theme = useMantineTheme();

  return (
    <>
      <Text fz="xl">{t("about")}</Text>
      <Text>
        &#169; Alexander Schoch, {new Date().getFullYear()}
        <br />
        <a
          style={{ color: theme.fn.primaryColor() }}
          href="mailto:schochal@student.ethz.ch"
        >
          schochal@student.ethz.ch
        </a>
        <br />
        CH – 8049 Zürich
        <br />
        <Group grow my={10}>
          <Button
            component="a"
            href="https://aschoch.ch"
            target="_blank"
            rightIcon={<IconHome size={20} />}
          >
            Homepage
          </Button>
          <Button
            rel="me"
            component="a"
            href="https://linuxrocks.online/@alexander_schoch"
            target="_blank"
            rightIcon={<IconBrandMastodon size={20} />}
          >
            Mastodon
          </Button>
        </Group>
      </Text>
      <Group grow mt={16}>
        <Button
          component="a"
          href="https://gitlab.com/alexander_schoch/homepage"
          target="_blank"
          rightIcon={<IconCode size={20} />}
        >
          Source Code
        </Button>
      </Group>
    </>
  );
}
