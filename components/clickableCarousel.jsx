import { useState } from "react";

import { Modal } from "@mantine/core";

import ImageCarousel from "../components/carousel";

function ClickableCarousel({ images }) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Modal opened={open} onClose={() => setOpen(false)} size="70%">
        <ImageCarousel images={images} setOpen={() => {}} />
      </Modal>
      <ImageCarousel images={images} setOpen={() => setOpen(true)} />
    </>
  );
}

export default ClickableCarousel;
