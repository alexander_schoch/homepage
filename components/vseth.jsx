import { useRouter } from "next/router";

import { Alert, Card, Container, Grid } from "@mantine/core";

import { IconInfoHexagonFilled } from "@tabler/icons-react";

import CVCard from "../components/cv_card";
import Section from "../components/section";

import vseth_de from "../content/cv/vseth.de.json";
import vseth_en from "../content/cv/vseth.en.json";

export default function VSETH() {
  const { locale } = useRouter();
  const vseth = locale == "de" ? vseth_de : vseth_en;

  return (
    <Container size="lg">
      <Section text="VSETH" />

      <Alert
        variant="filled"
        icon={<IconInfoHexagonFilled />}
        style={{ marginBottom: "20px" }}
      >
        {vseth.description}
      </Alert>

      <Grid>
        {vseth.positions.map((entry, index) => (
          <Grid.Col xs={12} sm={6} md={4} sx={{ display: "flex" }} key={index}>
            <Card sx={{ height: "100%" }}>
              <CVCard position={entry} showTime={true} />
            </Card>
          </Grid.Col>
        ))}
      </Grid>
    </Container>
  );
}
