import { useState } from "react";

import { MantineProvider, ColorSchemeProvider } from "@mantine/core";

import Shell from "../components/shell";
import theme from "../styles/theme";

export default function ThemeProvider(props) {
  const [colorScheme, setColorScheme] = useState(theme.colorScheme);
  const [themeColor, setThemeColor] = useState(theme.primaryColor);
  const toggleColorScheme = (value) =>
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));
  theme.colorScheme = colorScheme;
  theme.primaryColor = themeColor;

  const changeThemeColor = (color) => {
    setThemeColor(color);
  };

  return (
    <ColorSchemeProvider
      colorScheme={colorScheme}
      toggleColorScheme={toggleColorScheme}
    >
      <MantineProvider withGlobalStyles withNormalizeCSS theme={theme}>
        <Shell changeThemeColor={changeThemeColor}>{props.children}</Shell>
      </MantineProvider>
    </ColorSchemeProvider>
  );
}
