import { ColorSwatch, Group, Popover, useMantineTheme } from "@mantine/core";

export default function ColorPicker({ changeThemeColor }) {
  const theme = useMantineTheme();
  const c = theme.primaryColor;
  const i = theme.primaryShade[theme.colorScheme];

  const colors = Object.keys(theme.colors).map((c) => c);

  return (
    <Popover
      width={182}
      position="bottom"
      withArrow
      arrowPosition="side"
      shadow="md"
    >
      <Popover.Target>
        <ColorSwatch color={theme.colors[c][i]} />
      </Popover.Target>
      <Popover.Dropdown>
        <Group style={{ overflowX: "scroll", justifyContent: "center" }}>
          {colors.map((color) => (
            <ColorSwatch
              key={color}
              color={theme.colors[color][i]}
              onClick={() => changeThemeColor(color)}
            />
          ))}
        </Group>
      </Popover.Dropdown>
    </Popover>
  );
}
