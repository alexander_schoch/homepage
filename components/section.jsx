import React from "react";

import { Box, Group, Text, useMantineTheme } from "@mantine/core";

export default function Section({ text }) {
  const theme = useMantineTheme();

  return (
    <Box my={50}>
      <Group sx={{ justifyContent: "center" }}>
        <Text span sx={{ color: theme.fn.primaryColor() }} fz={32}>
          /&#42;
        </Text>
        <Text span variant="h2" fz={32}>
          {" "}
          {text}{" "}
        </Text>
        <Text span sx={{ color: theme.fn.primaryColor() }} fz={32}>
          &#42;/
        </Text>
      </Group>
    </Box>
  );
}
