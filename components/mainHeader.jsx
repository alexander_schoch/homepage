import Image from "next/image";

import { useTranslation } from "next-i18next";

import {
  Box,
  Card,
  Container,
  Grid,
  Text,
  useMantineTheme,
} from "@mantine/core";

export default function MainHeader() {
  const { t } = useTranslation("main");
  const theme = useMantineTheme();

  return (
    <Box sx={{ py: "20px" }}>
      <Container size="lg">
        <Box py={16}>
          <Card shadow="md" radius="md" p={0}>
            <Grid>
              <Grid.Col
                p={16}
                md={8}
                sm={8}
                xs={12}
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                <Box sx={{ margin: "auto" }}>
                  <Text fz={40} p sx={{ fontFamily: "cmu" }}>
                    {t("title")}
                    <Text
                      span
                      variant="gradient"
                      gradient={{
                        from: theme.fn.darken(theme.fn.primaryColor(), 0.3),
                        to: theme.fn.lighten(theme.fn.primaryColor(), 0.3),
                        deg: 40,
                      }}
                      sx={{ fontFamily: "cmu" }}
                      fw={700}
                    >
                      <span> Alexander Schoch</span>
                    </Text>
                    !
                  </Text>
                  <Text p c="dimmed" fz="xl" sx={{ fontFamily: "cmu" }}>
                    {t("subtitle")}
                  </Text>
                </Box>
              </Grid.Col>
              <Grid.Col md={4} sm={4} xs={12}>
                <div>
                  <Image
                    src="/images/alex.png"
                    width={0}
                    height={0}
                    sizes="100vw"
                    style={{
                      width: "100%",
                      height: "auto",
                      marginBottom: "-1rem",
                    }}
                    alt="Alexander Schoch"
                  />
                </div>
              </Grid.Col>
            </Grid>
          </Card>
        </Box>
      </Container>
    </Box>
  );
}
