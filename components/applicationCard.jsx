import { useTranslation } from "next-i18next";
import { IconExternalLink, IconBrandGitlab } from "@tabler/icons-react";

import { Badge, Box, Button, Card, Group, Text } from "@mantine/core";

export default function ApplicationCard({ app }) {
  const { t } = useTranslation("main");

  return (
    <Card
      withBorder
      shadow="md"
      radius="md"
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <Box>
        <Text fz="xl" mb="md">
          {app.name}
        </Text>
        <Text>{app.description}</Text>
      </Box>
      <Box>
        <Group my="20px" spacing="xs">
          {app.tags.map((tag) => (
            <Badge
              p={12}
              checked={false}
              variant="dot"
              color={tag.type === "Frontend" ? "blue" : "green"}
              key={tag.name}
            >
              {tag.name}
            </Badge>
          ))}
        </Group>
        <Group grow>
          {app.url != null && (
            <Button
              component="a"
              href={app.url}
              target="_blank"
              variant="filled"
              rightIcon={<IconExternalLink size={20} />}
            >
              {t("tryit")}
            </Button>
          )}
          <Button
            component="a"
            href={app.gitlab}
            target="_blank"
            variant="filled"
            rightIcon={<IconBrandGitlab size={20} />}
          >
            GitLab
          </Button>
        </Group>
      </Box>
    </Card>
  );
}
