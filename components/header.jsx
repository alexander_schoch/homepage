import React from "react";

import { Box, Container, Text, useMantineTheme } from "@mantine/core";

export default function Header({ text }) {
  const theme = useMantineTheme();

  return (
    <Box sx={{ py: "50px" }}>
      <Container>
        <Text
          variant="gradient"
          gradient={{
            from: theme.fn.darken(theme.fn.primaryColor(), 0.3),
            to: theme.fn.lighten(theme.fn.primaryColor(), 0.3),
            deg: 40,
          }}
          sx={{ textAlign: "center" }}
          fz={40}
          fw={700}
          my={20}
        >
          {text}
        </Text>
      </Container>
    </Box>
  );
}
