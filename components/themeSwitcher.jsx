import { Switch, useMantineColorScheme } from "@mantine/core";

import { IconSun, IconMoon } from "@tabler/icons-react";

export default function ThemeSwitcher() {
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  const dark = colorScheme === "dark";

  return (
    <Switch
      size="md"
      color="vsethMain"
      offLabel={<IconSun size={16} />}
      onLabel={<IconMoon size={16} />}
      checked={dark}
      onChange={() => {
        toggleColorScheme();
      }}
    />
  );
}
