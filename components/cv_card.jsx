import { Avatar, Badge, Box, Button, Group, Text } from "@mantine/core";

import {
  IconDownload,
  IconExternalLink,
  IconClockFilled,
} from "@tabler/icons-react";

export default function CVCard({
  position,
  showTitle = true,
  showTime = false,
}) {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "100%",
        height: "100%",
      }}
    >
      <Box>
        {showTitle && <Text fz="xl">{position.job}</Text>}
        <Text c="dimmed">{position.employer}</Text>
        {position.start && showTime && (
          <Box>
            <Badge
              my={10}
              pl={0}
              variant="filled"
              leftSection={
                <Avatar variant="default" size={20} mr={5}>
                  <IconClockFilled size={20} />
                </Avatar>
              }
            >
              {position.start +
                (position.end == position.start || !position.end
                  ? ""
                  : " - " + position.end)}
            </Badge>
          </Box>
        )}
        <Box>{position.description}</Box>
        {showTime || (
          <Text c="dimmed" fz="xs">
            {position.start == position.end
              ? position.start
              : position.start + " – " + position.end}
          </Text>
        )}
      </Box>
      <Box>
        {(position.scripts != null || position.website != null) && (
          <Group mt={5}>
            {position.scripts != null &&
              position.scripts.map((script) => (
                <Button
                  key={script.url}
                  component="a"
                  href={script.url}
                  rightIcon={<IconDownload size={20} />}
                >
                  {script.title}
                </Button>
              ))}
            {position.website != null && (
              <Button
                component="a"
                target="_blank"
                href={position.website}
                rightIcon={<IconExternalLink size={20} />}
              >
                Website
              </Button>
            )}
          </Group>
        )}
      </Box>
    </Box>
  );
}
