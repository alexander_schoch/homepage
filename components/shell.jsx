import Link from "next/link";
import { useRouter } from "next/router";

import { useState } from "react";

import { useTranslation } from "next-i18next";

import {
  AppShell,
  Box,
  Burger,
  Group,
  Header,
  MediaQuery,
  Navbar,
  SegmentedControl,
  Space,
  useMantineTheme,
} from "@mantine/core";

import { IconHome, IconMail, IconSchool, IconTools } from "@tabler/icons-react";

import Impressum from "../components/impressum";
import ThemeSwitcher from "../components/themeSwitcher";
import ColorPicker from "../components/colorPicker";

import { UnstyledButton } from "@mantine/core";

export default function Shell(props) {
  const { t } = useTranslation("navbar");
  const { asPath, locale, locales, pathname, push, query } = useRouter();
  const [opened, setOpened] = useState(false);
  const theme = useMantineTheme();

  const targets = [
    {
      text: t("home"),
      path: "/",
      icon: <IconHome color={theme.fn.primaryColor()} />,
    },
    {
      text: t("cv"),
      path: "/cv",
      icon: <IconSchool color={theme.fn.primaryColor()} />,
    },
    {
      text: t("projects"),
      path: "/projects",
      icon: <IconTools color={theme.fn.primaryColor()} />,
    },
    {
      text: t("contactMe"),
      path: "/contact",
      icon: <IconMail color={theme.fn.primaryColor()} />,
    },
  ];

  const handleLocChange = (l) => {
    if (l != locale) {
      push({ pathname, query }, asPath, { locale: l });
    }
  };

  const settings = (
    <Group sx={{ display: "flex", justifyContent: "center" }}>
      <ColorPicker changeThemeColor={props.changeThemeColor} />
      <ThemeSwitcher />
      <SegmentedControl
        value={locale}
        onChange={handleLocChange}
        data={locales.map((loc) => ({ label: loc.toUpperCase(), value: loc }))}
      />
    </Group>
  );

  return (
    <AppShell
      padding={0}
      navbarOffsetBreakpoint="sm"
      asideOffsetBreakpoint="sm"
      navbar={
        <Navbar
          width={{ base: 300 }}
          p="xs"
          hiddenBreakpoint="sm"
          hidden={!opened}
        >
          <Box
            sx={{
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Box>
              {targets.map((target, i) => (
                <Link
                  key={i}
                  href={target.path}
                  style={{ textDecoration: "none" }}
                >
                  <UnstyledButton
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      padding: "0.5rem 0.75rem",
                    }}
                  >
                    <span style={{ marginRight: "0.75rem" }}>
                      {target.icon}
                    </span>
                    <span style={{ flex: 1 }}>{target.text}</span>
                  </UnstyledButton>
                </Link>
              ))}
              <MediaQuery largerThan="sm" styles={{ display: "none" }}>
                <div>
                  <Space h="md" />
                  {settings}
                </div>
              </MediaQuery>
            </Box>
            <Box>
              <Impressum />
            </Box>
          </Box>
        </Navbar>
      }
      header={
        <Header height={75} p="xs" px={20}>
          <Group
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <MediaQuery largerThan="sm" styles={{ display: "none" }}>
              <Burger
                opened={opened}
                onClick={() => setOpened((o) => !o)}
                size="sm"
                color={theme.colors.gray[6]}
              />
            </MediaQuery>
            <Link href="/">
              <img
                src={
                  theme.colorScheme === "dark"
                    ? "/images/logo.svg"
                    : "/images/logo_dark.svg"
                }
                style={{ maxWidth: "150px", color: "red" }}
                alt="Logo"
              />
            </Link>
            <MediaQuery smallerThan="sm" styles={{ display: "none" }}>
              {settings}
            </MediaQuery>
          </Group>
        </Header>
      }
      sx={{
        padding: 0,
      }}
      styles={(theme) => ({
        main: {
          backgroundColor:
            theme.colorScheme === "dark"
              ? theme.colors.dark[8]
              : theme.colors.gray[0],
        },
      })}
    >
      {props.children}
    </AppShell>
  );
}
