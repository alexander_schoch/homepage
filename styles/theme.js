const theme = {
  colorScheme: 'dark',
  defaultRadius: 'sm',
  fontFamily: 'cmu',
  primaryColor: 'blue',
  /*colors: {
    blue: [
      '#e0efff',
      '#c0d5eb',
      '#a0bbd7',
      '#80a1c4',
      '#6087b0',
      '#406d9d',
      '#205389',
      '#003a76',
      '#00274f',
      '#001428'
    ]
  },
  primaryShade: 7*/
};

export default theme;
