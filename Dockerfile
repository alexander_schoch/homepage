FROM node:18

RUN mkdir -p /homepage
WORKDIR /homepage
COPY . /homepage

RUN npm install
RUN npm run build

EXPOSE 3000

CMD ["bash", "entrypoint.sh"]