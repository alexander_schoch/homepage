import Head from "next/head";
import React from "react";
import { useRouter } from "next/router";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import {
  Alert,
  Box,
  Button,
  Card,
  Container,
  Grid,
  Group,
  Space,
  Text,
  Timeline,
} from "@mantine/core";

import { IconBrandGitlab, IconInfoHexagonFilled } from "@tabler/icons-react";

import Header from "../components/header";
import Section from "../components/section";
import CVCard from "../components/cv_card";
import VSETH from "../components/vseth";

import cv_en from "../content/cv/cv.en.json";
import cv_de from "../content/cv/cv.de.json";

function Work({ data, title }) {
  return (
    <Box>
      <Container size="lg">
        <Section text={title} />

        <Timeline active={5} bulletSize={16}>
          {data.map((position, i) => (
            <Timeline.Item key={i} title={position.job}>
              <CVCard position={position} showTitle={false} />
            </Timeline.Item>
          ))}
        </Timeline>
      </Container>
    </Box>
  );
}

function Education() {
  const { t } = useTranslation(["cv", "common"]);

  const { locale } = useRouter();
  const cv = locale == "de" ? cv_de : cv_en;

  return (
    <Box>
      <Container>
        <Section text={t("education")} />

        <Timeline active={2} bulletSize={16}>
          {cv.education.map((school, i) => (
            <Timeline.Item key={i} title={school.name}>
              <Text c="dimmed">
                {school.institution + ", " + school.address}
              </Text>
              {school.spf != null && (
                <Box mt={5}>
                  <Text>Schwerpunktfach: {school.spf}</Text>
                  <Text>Ergänzungsfach: {school.ef}</Text>
                  <Text>Wahlpflichtfach: {school.wpf}</Text>
                </Box>
              )}
              <Text c="dimmed" fz="xs">
                {school.start} – {school.end}
              </Text>
            </Timeline.Item>
          ))}
        </Timeline>
      </Container>
    </Box>
  );
}

function ComputerSkills({ data }) {
  const { t } = useTranslation(["cv", "common"]);

  return (
    <Box>
      <Container size="lg">
        <Section text={t("software_skills")} />

        <Alert variant="filled" icon={<IconInfoHexagonFilled />}>
          {data.open_source}
        </Alert>
        <Space h="md" />

        <Grid>
          <Grid.Col
            md={4}
            sm={6}
            xs={12}
            sx={{
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <Card sx={{ height: "100%" }}>
              <Text fz="xl">{t("programming")}</Text>
              <Box>
                {t("webdev")}:
                <ul>
                  {data.programming.webdev.map((e) => (
                    <li key={e.framework}>
                      {e.framework} ({e.language})
                    </li>
                  ))}
                </ul>
                {t("gamedev")}:
                <ul>
                  {data.programming.gamedev.map((e) => (
                    <li key={e.framework}>
                      {e.framework} ({e.language})
                    </li>
                  ))}
                </ul>
                {t("appdev")}:
                <ul>
                  {data.programming.appdev.map((e) => (
                    <li key={e.framework}>
                      {e.framework} ({e.language})
                    </li>
                  ))}
                </ul>
              </Box>
              <Group>
                <Button
                  component="a"
                  href={data.programming.gitlab}
                  rightIcon={<IconBrandGitlab size={20} />}
                >
                  GitLab
                </Button>
                <Button
                  component="a"
                  href={data.programming.gitlabeth}
                  rightIcon={<IconBrandGitlab size={20} />}
                >
                  GitLab ETH
                </Button>
              </Group>
            </Card>
          </Grid.Col>

          <Grid.Col md={4} sm={6} xs={12}>
            <Card sx={{ height: "100%" }}>
              <Text fz="xl">{t("sysadmin")}</Text>
              <Box>
                <ul>
                  {data.sysadmin.map((e) => (
                    <li key={e.name}>{e.name}</li>
                  ))}
                </ul>
              </Box>
            </Card>
          </Grid.Col>

          <Grid.Col md={4} sm={6} xs={12}>
            <Card sx={{ height: "100%" }}>
              <Text fz="xl">{t("apps")}</Text>
              <Box>
                <ul>
                  {data.applications.map((e) => (
                    <li key={e.name}>
                      {e.use}: {e.name}
                    </li>
                  ))}
                </ul>
              </Box>
            </Card>
          </Grid.Col>
        </Grid>
      </Container>
    </Box>
  );
}

function Military({ data }) {
  const { t } = useTranslation(["cv", "common"]);

  return (
    <Box>
      <Container size="lg">
        <Section text={t("army")} />

        <Timeline active={2} bulletSize={16}>
          {data.map((e, i) => (
            <Timeline.Item title={e.function} key={i}>
              <Text c="dimmed">{e.type}</Text>
              <Text size="sm" c="dimmed">
                {e.description}
              </Text>
              <Text size="xs" mt={4}>
                {e.year}
              </Text>
            </Timeline.Item>
          ))}
        </Timeline>
      </Container>
    </Box>
  );
}

function CV() {
  const { t } = useTranslation(["cv", "common"]);

  const { locale } = useRouter();
  const cv = locale == "de" ? cv_de : cv_en;

  return (
    <>
      <Head>
        <title>CV | Alexander Schoch</title>
      </Head>
      <Header text={t("curriculum_vitae")} />
      <Education />
      <ComputerSkills data={cv.computer_skills} />
      <Work title={t("work")} data={cv.work} />
      <VSETH />
      <Military data={cv.military} />
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "cv",
        "common",
        "navbar",
        "footer",
      ])),
    },
  };
}

export default CV;
