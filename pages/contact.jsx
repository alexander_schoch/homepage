import Head from "next/head";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { Container } from "@mantine/core";

import Header from "../components/header";
import ContactForm from "../components/contactForm";

export default function Contact() {
  const { t } = useTranslation(["contact"]);

  return (
    <>
      <Head>
        <title>Contact Me | Alexander Schoch</title>
      </Head>
      <Header text={t("contactMe")} />
      <Container size="lg">
        <ContactForm />
      </Container>
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "navbar",
        "footer",
        "contact",
      ])),
    },
  };
}
