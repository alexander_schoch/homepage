import React from "react";
import Head from "next/head";
import { useRouter } from "next/router";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { Box, Container, Grid } from "@mantine/core";

import applications_en from "../content/applications.en.json";
import applications_de from "../content/applications.de.json";

import Section from "../components/section";
import ApplicationCard from "../components/applicationCard";
import MainHeader from "../components/mainHeader";

function Main() {
  const { locale } = useRouter();
  const { t } = useTranslation("main");
  const applications = locale == "de" ? applications_de : applications_en;

  return (
    <>
      <Head>
        <title>Home | Alexander Schoch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <MainHeader />
      <Box style={{ marginTop: "20px" }}>
        <Container size="lg">
          <Section text={t("myWebapps")} />
          <Grid sx={{ justifyContent: "center" }}>
            {applications.applications.map((app, i) => (
              <Grid.Col
                md={4}
                sm={6}
                xs={12}
                key={i}
                style={{ display: "flex" }}
              >
                <ApplicationCard app={app} />
              </Grid.Col>
            ))}
          </Grid>
        </Container>
      </Box>
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "main",
        "common",
        "navbar",
        "footer",
      ])),
    },
  };
}

export default Main;
