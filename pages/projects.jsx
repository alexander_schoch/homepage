import Head from "next/head";
import { useRouter } from "next/router";

import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { Button, Container, Grid, Group, Text } from "@mantine/core";

import { IconExternalLink, IconBrandGitlab } from "@tabler/icons-react";

import Section from "../components/section";
import Header from "../components/header";
import ClickableCarousel from "../components/clickableCarousel";

import projects from "../content/projects";

export default function Projects() {
  const { locale } = useRouter();
  const { t } = useTranslation(["projects"]);

  return (
    <>
      <Head>
        <title>Projects | Alexander Schoch</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Header text={t("title")} />
      <Container size="lg">
        {projects.map((project, i) => (
          <div key={i}>
            <Section
              text={locale == "de" ? project.title_de : project.title_en}
            />
            <Grid>
              <Grid.Col md={6} xs={12}>
                <ClickableCarousel images={project.images} />
              </Grid.Col>
              <Grid.Col md={6} xs={12}>
                <Text sx={{ whiteSpace: "pre-line" }}>
                  {locale == "de"
                    ? project.description_de
                    : project.description_en}
                </Text>
                <Text sx={{ marginTop: "20px" }}>
                  <b>{t("tech_stack")}</b>: {project.tech_stack.join(", ")}
                </Text>
                <Group grow my={16}>
                  <Button
                    component="a"
                    href={project.url}
                    rightIcon={<IconExternalLink size={20} />}
                    target="_blank"
                  >
                    {t("try_it")}
                  </Button>
                  <Button
                    component="a"
                    href={project.source_code}
                    rightIcon={<IconBrandGitlab size={20} />}
                    target="_blank"
                  >
                    Code
                  </Button>
                </Group>
              </Grid.Col>
            </Grid>
          </div>
        ))}
      </Container>
    </>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "navbar",
        "footer",
        "projects",
      ])),
    },
  };
}
