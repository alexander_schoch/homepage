// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import nodemailer from "nodemailer";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  if (req.method == "POST") {
    const { name, mail, message, human, robot } = req.body;

    if (
      human == true &&
      robot == false &&
      name != "" &&
      mail != "" &&
      message != ""
    ) {
      const transporter = nodemailer.createTransport({
        host: process.env.MAILER_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.MAILER_USERNAME,
          pass: process.env.MAILER_PASSWORD,
        },
      });

      await transporter.sendMail({
        from: process.env.MAILER_ADDRESS,
        to: process.env.MAILER_ADDRESS,
        replyTo: mail,
        subject: (process.env.MAILER_SUBJECT || "").replace("NAME", name),
        html: message,
      });

      res.status(200).json({ message: "mailSuccess" } as any);
    } else {
      res.status(200).json({ message: "mailFailure" } as any);
    }
  } else {
    res.setHeader("Allow", ["POST"]);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
