import "../styles/globals.css";
import Head from "next/head";

import { useState } from "react";

import { MantineProvider, ColorSchemeProvider } from "@mantine/core";
import { Notifications } from "@mantine/notifications";

import Shell from "../components/shell";
import theme from "../styles/theme";

import { appWithTranslation } from "next-i18next";

function App({ Component, pageProps }) {
  const [colorScheme, setColorScheme] = useState(theme.colorScheme);
  const [themeColor, setThemeColor] = useState(theme.primaryColor);
  const toggleColorScheme = (value) =>
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));
  theme.colorScheme = colorScheme;
  theme.primaryColor = themeColor;

  const changeThemeColor = (color) => {
    setThemeColor(color);
  };
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
        justifyContent: "space-between",
      }}
    >
      <Head>
        <link rel="shortcut icon" href="/favicon.png" />
      </Head>
      <ColorSchemeProvider
        colorScheme={colorScheme}
        toggleColorScheme={toggleColorScheme}
      >
        <MantineProvider withGlobalStyles withNormalizeCSS theme={theme}>
          <Notifications />

          <Shell changeThemeColor={changeThemeColor}>
            <Component {...pageProps} />
          </Shell>
        </MantineProvider>
      </ColorSchemeProvider>
    </div>
  );
}

export default appWithTranslation(App);
