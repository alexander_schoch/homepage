import tasting from './01_tasting.json';
import newsletter from './02_newsletter.json';
import papperlaweb from './03_papperlaweb.json';

const projects = [
  papperlaweb,
  newsletter,
  tasting,
]

export default projects;
